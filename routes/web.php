<?php
$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/check', ['uses' => 'CommonController@check_mail']);

$router->post('/user', ['uses' => 'CommonController@user']);

$router->get('/user/info', ['uses' => 'CommonController@check_info']);

$router->put('/user/info', ['uses' => 'CommonController@modif_info']);

$router->put('/user/pass', ['uses' => 'CommonController@modif_pass']);

$router->get('/user/votes',['uses' => 'CommonController@get_user_votes']);

$router->get("/framework/best", ['uses' => 'FrameworkController@get_best_framework']);

$router->get("/frameworks", ['uses' => 'FrameworkController@get_frameworks']);

$router->post('/note', ['uses' => 'NoteController@create_note']);

$router->post("/contact",['uses' => 'ContactController@create_contact']);