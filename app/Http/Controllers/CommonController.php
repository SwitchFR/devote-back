<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;

use App\User;
use App\Session;
use App\Note;
use App\Framework;

class CommonController extends BaseController
{
    public function check_mail(Request $request)
    {
        $user = User::where("mail",$request->input("mail"))->first();

        if(isset($user))
        {
            return response(array("success" => false), 400)->header('Content-Type', "application/json");
        }
        else
        {
            return response(array("success" => true), 200)->header('Content-Type', "application/json");
        }
    }

    public function user(Request $request)
    {
        if($request->has("repass"))
        {
            if($request->has("login") && $request->has("pass") && $request->has("fname") && $request->has("name"))
            {
                $user = new User();

                $user->mail = $request->input("login");
                $user->pass = md5($request->input("pass"));
                $user->prenom = $request->input("fname");
                $user->nom = $request->input("name");
                $user->role = 1;

                if($user->save())
                {
                    $session = new Session();
                    $session->user = $user->id;
                    unset($user->pass);
                    $session->token = base64_encode($user);

                    if($session->save())
                    {
                        return response(array("success" => true,"token" => $session->token), 200)->header('Content-Type', "application/json");
                    }
                }
                else
                {
                    return response(array("success" => false,"error" => "Création impossible"), 500)->header('Content-Type', "application/json");
                }
            }
        }
        else
        {
            if($request->has("login") && $request->has("pass"))
            {
                $login = $request->input("login");
                $pass = md5($request->input("pass"));
    
                $user = User::where("mail",$login)->where("pass",$pass)->first();
                if(isset($user))
                {
                    $session = new Session();
                    $session->user = $user->id;
                    unset($user->id);
                    unset($user->pass);
                    unset($user->role);
                    $session->token = base64_encode($user);

                    if($session->save())
                    {
                        return response(array("success" => true,"token" => $session->token), 200)->header('Content-Type', "application/json");
                    }
                    else
                    {
                        return response(array("success" => false , "error" => "Erreur création token !"), 500)->header('Content-Type', "application/json");
                    }
                }
                else
                {
                    return response(array("success" => false , "error" => "Identifiants incorrects !","debug" => md5("test01")), 500)->header('Content-Type', "application/json");
                }
            }
            else
            {
                return response(array("success" => false,"error" => "parametre manquants"), 400)->header('Content-Type', "application/json");
            }
        }
    }

    public function check_info(Request $request)
    {
        if($request->has("token"))
        {
            $session = Session::where("token",$request->input("token"))->first();
            if(isset($session))
            {
                $user = User::where("id",$session->user)->first();
                unset($user->id);
                unset($user->pass);
                unset($user->role);

                $notes = Note::where("user_id",$session->user)->get();

                foreach($notes as $uneNote)
                {
                    $uneNote->element = Framework::where("id",$uneNote->element)->first()["nom"];
                }
                
                if(isset($notes))
                {
                    return response(array("success" => true,"result" => $user,"notes" => $notes), 200)->header('Content-Type', "application/json");
                }
                else
                {
                    return response(array("success" => true,"result" => $user), 200)->header('Content-Type', "application/json");
                }
            }
            else
            {
                return response(array("success" => false,"error" => "Token inconnu"), 500)->header('Content-Type', "application/json");
            }
            return response(array("success" => true,"token" => $session->token), 200)->header('Content-Type', "application/json");
        }
        else
        {
            return response(array("success" => false,"error" => "parametre manquants"), 400)->header('Content-Type', "application/json");
        }
    }

    public function modif_info(Request $request)
    {
        if($request->has("token") && $request->has("mail") && $request->has("fname") && $request->has("name"))
        {
            $session = Session::where("token",$request->input("token"))->first();
            if(isset($session))
            {
                $user = User::where("id",$session->user)->first();
                $user->mail = $request->input("mail");
                $user->prenom = $request->input("fname");
                $user->nom = $request->input("name");

                if($user->save())
                {
                    return response(array("success" => true), 200)->header('Content-Type', "application/json");
                }
                else
                {
                    return response(array("success" => false,"error" => "Impossible de modifier les infos !"), 500)->header('Content-Type', "application/json");
                }
            }
            else
            {
                return response(array("success" => false,"error" => "Token inconnu"), 500)->header('Content-Type', "application/json");
            }
            return response(array("success" => true,"token" => $session->token), 200)->header('Content-Type', "application/json");
        }
        else
        {
            return response(array("success" => false,"error" => "parametre manquants"), 400)->header('Content-Type', "application/json");
        }
    }

    public function modif_pass(Request $request)
    {
        if($request->has("token") && $request->has("pass"))
        {
            $session = Session::where("token",$request->input("token"))->first();
            if(isset($session))
            {
                $user = User::where("id",$session->user)->first();
                $user->pass = md5($request->input("pass"));

                if($user->save())
                {
                    return response(array("success" => true), 200)->header('Content-Type', "application/json");
                }
                else
                {
                    return response(array("success" => false,"error" => "Impossible de modifier les infos !"), 500)->header('Content-Type', "application/json");
                }
            }
            else
            {
                return response(array("success" => false,"error" => "Token inconnu"), 500)->header('Content-Type', "application/json");
            }
        }
        else
        {
            return response(array("success" => false,"error" => "parametre manquants"), 400)->header('Content-Type', "application/json");
        }
    }

    public function get_user_votes(Request $request)
    {
        if($request->has("token"))
        {
            $session = Session::where("token",$request->input("token"))->first();
            if(isset($session))
            {
                $notes = Note::where("user_id",$session->user)->get();
                if($request->has("framework"))
                {
                    $notes = Note::where("user_id",$session->user)->where('element',$request->input("framework"))->first();
                }
                if(isset($notes))
                {
                    return response(array("success" => true,"result" => $notes), 200)->header('Content-Type', "application/json");
                }
                else
                {
                    return response(array("success" => true), 200)->header('Content-Type', "application/json");
                }
            }
            else
            {
                return response(array("success" => false,"error" => "Token inconnu"), 500)->header('Content-Type', "application/json");
            }
        }
        else
        {
            return response(array("success" => false,"error" => "Parametre manquants"), 400)->header('Content-Type', "application/json");
        }
    }

}
