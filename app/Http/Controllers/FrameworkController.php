<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;

use App\Framework;
use App\Session;
use App\Note;

class FrameworkController extends BaseController
{
    public function get_best_framework(Request $request)
    {
        $max = 0;
        foreach(Framework::all() as $unFramework)
        {
            $size =  Note::where("element",$unFramework->id)->get()->count();
            if($size != 0)
            {
                $notes = Note::where("element",$unFramework->id)->get()->sum("note");
                $moyenne = $notes/$size;
                if($moyenne > $max)
                {
                    $bestFramework = $unFramework;
                    $max = $moyenne;
                }
            }
        }

        if(isset($bestFramework))
        {
            return response(array("success" => true,"result" => $bestFramework), 200)->header('Content-Type', "application/json");
        }
        else
        {
            return response(array("success" => false , "error" => "Framework introuvable !"), 404)->header('Content-Type', "application/json");
        }
    }

    public function get_frameworks(Request $request)
    {
        $Framework = Framework::get();

        if($request->has("token"))
        {
            $session = Session::where("token",$request->input("token"))->first();
            
            foreach($Framework as $unFramework)
            {
                $note = Note::where("element",$unFramework->id)->where("user_id",$session->user)->first();
                if(isset($note))
                {
                    $unFramework->note = $note;
                }
            }
        }

        if(isset($Framework))
        {
            return response(array("success" => true,"result" => $Framework), 200)->header('Content-Type', "application/json");
        }
        else
        {
            return response(array("success" => false , "error" => "Framework introuvable !"), 404)->header('Content-Type', "application/json");
        }
    }

}
