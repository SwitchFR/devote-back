<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;

use App\Note;
use App\Session;

class NoteController extends BaseController
{
    public function create_note(Request $request)
    {
        if($request->has("token") && $request->has("note") && $request->has("framework"))
        {
            $session = Session::where("token",$request->input("token"))->first();

            if(isset($session))
            {
                $note = new Note();
                $note->note = $request->input("note");
                $note->user_id = $session->user;
                $note->element = $request->input("framework");
                $note->message = "";
                if($request->has("text"))
                {
                    $note->message = $request->input("text");
                }
                if($note->save())
                {
                    return response(array("success" => true), 200)->header('Content-Type', "application/json");
                }
                else
                {
                    return response(array("success" => false , "error" => "Impossible de sauvegarder la note !"), 500)->header('Content-Type', "application/json");
                }
            }
            else
            {
                return response(array("success" => false , "error" => "Utilisateur introuvable !"), 404)->header('Content-Type', "application/json");
            }
        }
        else
        {
            return response(array("success" => false , "error" => "Parametre manquants !","debug" => $request->all()), 400)->header('Content-Type', "application/json");
        }
    }
}
