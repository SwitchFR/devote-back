<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;

use App\Contact;

class ContactController extends BaseController
{
    public function create_contact(Request $request)
    {
        if($request->has("nom") && $request->has("mail") && $request->has("message"))
        {
            $Contact = new Contact();
            $Contact->nom = $request->input("nom");
            $Contact->mail = $request->input("mail");
            $Contact->message = $request->input("message");

            if($Contact->save())
            {
                return response(array("success" => true), 200)->header('Content-Type', "application/json");
            }
            else
            {
                return response(array("success" => false , "error" => "Sauvegarde du formulaire impossible !"), 500)->header('Content-Type', "application/json");
            }
        }
        else
        {
            return response(array("success" => false , "error" => "Parametre manquants !"), 400)->header('Content-Type', "application/json");
        }
    }
}
